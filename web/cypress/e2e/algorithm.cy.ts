import {AlgorithmPage} from '../pages/algorithm-page'
describe('Algorithm page tests', () => {
  /**
   * Страница с алгоритмом
   */
  let page = new AlgorithmPage()

  /**
   * Setup
   */
  beforeEach(() => {
    cy.visit('/')
  })

  /**
   * Проверка на то, что при правильных данных алгоритм работает (вернёт корректный палиндром)
   */
  it('Send valid string test', () => {
    page.solveAlgorithm('abada')
    page.shouldOutput('aba')
  })

  /**
   * Проверка на то, что отправка строки с одинаковыми символами вернёт
   * корректный палиндром (саму исходную строку)
   */
  it('Send string of identical chars test', () => {
    page.solveAlgorithm('bbbbb')
    page.shouldOutput('bbbbb')
  })

  /**
   * Проверка на то, что отправка строки с одинаковыми символами вернёт
   * корректный палиндром (саму исходную строку)
   */
  it('Send very long acceptable string test', () => {
    page.solveAlgorithmWithLongString('01', 500)
    page.shouldOutput('0')
  })

  /**
   * Проверка на то, что пустая строка вернёт ошибку
   */
  it('Send empty string test', () => {
    page.solveAlgorithm()
    page.shouldError()
  })

  /**
   * Проверка на то, что отправка строки со спец. символами вернёт ошибку
   */
  it('Send string with special chars test', () => {
    page.solveAlgorithm('% percent')
    page.shouldError()
  })

  /**
   * Проверка на то, что отправка строки с управляющими кодами вернёт ошибку
   */
  it('Send string with control codes test', () => {
    page.solveAlgorithm('\0 test')
    page.shouldOutput('Ошибка')
  })

  /**
   * Проверка на то, что отправка строки с символами других алфавитов (не латинского алфавита) вернёт ошибку
   */
  it('Send string with non-latin chars test', () => {
    page.solveAlgorithm('Привет ffdjf')
    page.shouldError()
  })

  /**
   * Проверка на то, что отправка очень большой строки вернёт ошибку
   */
  it('Send very long unacceptable string test', () => {
    page.solveAlgorithmWithLongString('0', 1001)
    page.shouldError()
  })
})
