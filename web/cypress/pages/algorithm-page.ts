export class AlgorithmPage {
  elements = {
    inputText: () => cy.get('#input'),
    submitButton: () => cy.get('#submit'),
    outputParagraph: () => cy.get('#output'),
  }

  solveAlgorithm(input?: string) {
    cy.intercept({
      method: 'Post',
      url: '/algorithm'
    }).as('request')

    if (input != null) {
      this.elements.inputText().type(input)
    }
    this.elements.submitButton().click()
  }

  solveAlgorithmWithLongString(substring: string, length: number) {
    let string = ''
    for (let i = 0; i < length; i++) {
      string = string + substring
    }
    this.solveAlgorithm(string)
  }

  shouldOutput(text: string) {
    cy.wait(['@request']).then(() => {
      this.elements.outputParagraph().contains(text)
    })
  }

  shouldError() {
    this.shouldOutput('Ошибка')
  }
}
