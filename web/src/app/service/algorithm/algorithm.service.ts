import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ResponseDto} from "../../dto/response-dto";
import {RequestDto} from "../../dto/request-dto";

@Injectable({
  providedIn: 'root'
})
export class AlgorithmService {

  constructor(
    private httpClient: HttpClient
  ) { }

  eval(string: string): Observable<ResponseDto> {
    return this.httpClient.post<ResponseDto>(
      "http://localhost:8080/algorithm",
      new RequestDto(string)
    )
  }
}
