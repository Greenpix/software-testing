import { Component } from '@angular/core';
import {AlgorithmService} from "../../service/algorithm/algorithm.service";

@Component({
  selector: 'app-algorithm',
  templateUrl: './algorithm.component.html',
  styleUrls: ['./algorithm.component.css']
})
export class AlgorithmComponent {

  input: string = ""
  output: string = ""

  constructor(
    private service: AlgorithmService
  ) {}

  onSubmit() {
    this.service.eval(this.input).subscribe({
      next: res => {
        this.output = res.string
      },
      error: err => {
        this.output = "Ошибка"
      }
    })
  }

}
