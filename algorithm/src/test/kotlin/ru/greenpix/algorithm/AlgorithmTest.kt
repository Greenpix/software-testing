package ru.greenpix.algorithm

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.junit.jupiter.params.provider.ValueSource
import kotlin.test.assertEquals

class AlgorithmTest {

    private lateinit var algorithm: Algorithm

    /**
     * Аналог setup в питоне. Выполняется для каждого теста.
     */
    @BeforeEach
    fun setup() {
        algorithm = AlgorithmImpl()
    }

    /**
     * Проверяем, что наш алгоритм работает корректно при допустимых значениях.
     * Параметризованный тест
     */
    @DisplayName("Позитивные тесты")
    @ParameterizedTest
    @CsvSource(
        "babad,bab",
        "cbbd,bb",
        "0,0",
        "aZa,aZa"
    )
    fun positiveTest(input: String, expected: String) {
        val actual = algorithm.longestPalindrome(input)
        assertEquals(expected, actual)
    }

    /**
     * Проверяем, что наш алгоритм работает корректно,
     * если на вход подать огромную строку в пределах допустимых значений
     */
    @DisplayName("Позитивный тест с большой допустимой строкой")
    @Test
    fun positiveLongStringTest() {
        val longString = Helper.repeatString("a", 1000)
        val actual = algorithm.longestPalindrome(longString)
        assertEquals(longString, actual)
    }

    /**
     * Проверяем, что наш алгоритм выбрасывает ошибки при недопустимых значениях.
     * Параметризованный тест
     */
    @DisplayName("Негативный тест с недопустимыми значениями")
    @ParameterizedTest
    @ValueSource(strings = [
        "",
        "%",
        "a$",
        "и",
        "1в",
        "\u0000"
    ])
    fun invalidParamsTest(input: String) {
        assertThrows<IllegalArgumentException> { algorithm.longestPalindrome(input) }
    }

    /**
     * Проверяем, что наш алгоритм работает корректно,
     * если на вход подать огромную строку в пределах допустимых значений
     */
    @DisplayName("Негативный тест со слишком большой строкой")
    @Test
    fun negativeLongStringTest() {
        assertThrows<IllegalArgumentException> {
            algorithm.longestPalindrome(Helper.repeatString("a", 1001))
        }
    }
}