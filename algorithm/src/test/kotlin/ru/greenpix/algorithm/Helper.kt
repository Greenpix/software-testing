package ru.greenpix.algorithm

object Helper {

    /**
     * Возвращает строку, которая является результатом конкатенации
     * строки [string] с самой собой [times] раз
     *
     * Пример:
     * ```kotlin
     * val a = repeatString("abc", 3)
     * println(a)
     * ```
     * Вывод
     * ```
     * abcabcabc
     * ```
     */
    fun repeatString(string: String, times: Int): String {
        val builder = StringBuilder()
        for (i in 1..times) {
            builder.append(string)
        }
        return builder.toString()
    }

}