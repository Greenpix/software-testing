package ru.greenpix.algorithm

interface Algorithm {

    fun longestPalindrome(string: String): String

}