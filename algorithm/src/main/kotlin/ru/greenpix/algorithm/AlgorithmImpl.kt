package ru.greenpix.algorithm

class AlgorithmImpl : Algorithm {

    private companion object {
        const val ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    }

    override fun longestPalindrome(string: String): String {
        if (string.length !in 1..1000) {
            throw IllegalArgumentException("Max length of string must be in range 1..1000")
        }

        string.forEach {
            if (it !in ALPHABET) {
                throw IllegalArgumentException("The string can only contain latin letters and numbers")
            }
        }

        var res = ""
        var resLen = 0

        for (i in string.indices) {
            var l = i
            var r = i

            // odd length
            while (l >= 0 && r < string.length && string[l] == string[r]) {
                if (r - l + 1 > resLen) {
                    res = string.substring(l .. r)
                    resLen = r - l + 1
                }
                l -= 1
                r += 1
            }

            // even length
            l = i
            r = i + 1
            while (l >= 0 && r < string.length && string[l] == string[r]) {
                if (r - l + 1 > resLen) {
                    res = string.substring(l .. r)
                    resLen = r - l + 1
                }
                l -= 1
                r += 1
            }
        }

        return res
    }
}