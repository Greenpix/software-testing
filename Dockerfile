FROM maven:3-amazoncorretto-17

WORKDIR /app/algorithm
COPY algorithm/src src
COPY algorithm/pom.xml .

RUN mvn install

WORKDIR /app/api
COPY api/src src
COPY api/pom.xml .

CMD mvn verify