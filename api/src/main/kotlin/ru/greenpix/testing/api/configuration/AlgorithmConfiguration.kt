package ru.greenpix.testing.api.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.greenpix.algorithm.Algorithm
import ru.greenpix.algorithm.AlgorithmImpl

@Configuration
class AlgorithmConfiguration {

    @Bean
    fun solution(): Algorithm = AlgorithmImpl()

}