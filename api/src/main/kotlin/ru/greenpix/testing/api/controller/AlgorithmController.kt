package ru.greenpix.testing.api.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.greenpix.algorithm.Algorithm
import ru.greenpix.testing.api.dto.RequestDto
import ru.greenpix.testing.api.dto.ResponseDto

@RestController
@RequestMapping("algorithm")
class AlgorithmController(
    private val algorithm: Algorithm
) {

    @PostMapping
    fun execute(
        @RequestBody
        requestDto: RequestDto
    ): ResponseEntity<ResponseDto> {
        return try {
            ResponseEntity.ok(ResponseDto(string = algorithm.longestPalindrome(requestDto.string)))
        }
        catch (e: IllegalArgumentException) {
            ResponseEntity.badRequest().build()
        }
    }

}