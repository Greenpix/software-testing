package ru.greenpix.testing.api

import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.junit.jupiter.params.provider.ValueSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.post

@SpringBootTest
@AutoConfigureMockMvc
class AlgorithmControllerTest {

    /**
     * Имитирует отправку HTTP запроса в приложение.
     * Встроенный инструмент библиотеки spring-boot-starter-test.
     */
    @Autowired
    lateinit var mockMvc: MockMvc

    @DisplayName("Позитивная проверка отправки тела в POST методе")
    @Test
    fun postHttpStatus() {
        mockMvc.post("/algorithm") {
            contentType = MediaType.APPLICATION_JSON
            content = "{\"string\": \"string\"}"
        }.andExpect { status { isOk() } }
    }

    @DisplayName("Негативная проверка отправки пустого тела в POST методе")
    @Test
    fun postEmptyBodyHttpStatus() {
        mockMvc.post("/algorithm") {
            contentType = MediaType.APPLICATION_JSON
        }.andExpect { status { isBadRequest() } }
    }

    @DisplayName("Негативная проверка отправки некорректного тела в POST методе")
    @Test
    fun postInvalidBodyBodyHttpStatus() {
        mockMvc.post("/algorithm") {
            contentType = MediaType.APPLICATION_JSON
            content = "{\"invalid\": \"invalid\"}"
        }.andExpect { status { isBadRequest() } }
    }


    /**
     * Проверяем, что наш алгоритм работает корректно при допустимых значениях.
     * Параметризованный тест
     */
    @DisplayName("Позитивные тесты")
    @ParameterizedTest
    @CsvSource(
        "k,k",
        "g0g0f,g0g",
        "46ffi4,ff",
        "k9k9k,k9k9k"
    )
    fun positiveTest(input: String, expected: String) {
        mockMvc.post("/algorithm") {
            contentType = MediaType.APPLICATION_JSON
            content = "{\"string\": \"$input\"}"
        }.andExpect { content {
            json("{string: \"$expected\"}")
        } }
    }

    /**
     * Проверяем, что наш алгоритм работает корректно,
     * если на вход подать огромную строку в пределах допустимых значений
     */
    @DisplayName("Позитивный тест с большой допустимой строкой")
    @Test
    fun positiveLongStringTest() {
        val longString = Helper.repeatString("0", 1000)
        mockMvc.post("/algorithm") {
            contentType = MediaType.APPLICATION_JSON
            content = "{\"string\": \"$longString\"}"
        }.andExpect { content {
            json("{string: \"$longString\"}")
        } }
    }

    /**
     * Проверяем, что наш алгоритм выбрасывает ошибки при недопустимых значениях.
     * Параметризованный тест
     */
    @DisplayName("Негативный тест с недопустимыми значениями")
    @ParameterizedTest
    @ValueSource(strings = [
        "",
        "$",
        "&h",
        "в",
        "лb",
        "\u0001"
    ])
    fun invalidParamsTest(input: String) {
        mockMvc.post("/algorithm") {
            contentType = MediaType.APPLICATION_JSON
            content = "{\"string\": \"$input\"}"
        }.andExpect { status { isBadRequest() } }
    }

    /**
     * Проверяем, что наш алгоритм работает корректно,
     * если на вход подать огромную строку в пределах допустимых значений
     */
    @DisplayName("Негативный тест со слишком большой строкой")
    @Test
    fun negativeLongStringTest() {
        val longString = Helper.repeatString("0", 1001)
        mockMvc.post("/algorithm") {
            contentType = MediaType.APPLICATION_JSON
            content = "{\"string\": \"$longString\"}"
        }.andExpect { status { isBadRequest() } }
    }
}
