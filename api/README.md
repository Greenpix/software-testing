# Задание № 2 - второй уровень пирамиды тестирования
## О решении
Решение выполнено с использованием языка программирования Kotlin, веб фреймворка Spring и фреймворка JUnit для тестирования.

## Директория тестов
Тесты находятся в директории [src/test](src/test/kotlin/ru/greenpix/testing/api)

## Запуск тестов
Требуется установленный docker. Запустите Dockerfile,
расположенный в **корневой директории всего репозитория**.
Ссылка на [Dockerfile](../Dockerfile). 
Команда (если запускать из директории этого модуля): `docker run ../`

Ожидаемый вывод:
![img.png](../.docs/images/mvn-test.png)

## Баг репорты
Ссылка на [баг репорты](.docs/BUG_REPORT.md)